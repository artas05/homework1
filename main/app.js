let second, first;

function distance(first, second) {
  //TODO: implementați funcția
  // TODO: implement the function

  if (first instanceof Array && second instanceof Array) {
    if (first.length == 0 && second.length == 0) {
      return 0;
    }

    first = [...new Set(first)];
    second = [...new Set(second)];

    for (let i = 0; i < first.length; i++) {
      for (let j = 0; j < second.length; j++) {
        if (first[i] === second[j]) {
          first.splice(i, 1);
          second.splice(j, 1);
        }
      }
    }

    return first.length + second.length;
  } else {
    throw new Error("InvalidType");
  }
}

module.exports.distance = distance;
